package com.mediagenix.technicaltestjozef.web;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mediagenix.technicaltestjozef.model.Book;
import com.mediagenix.technicaltestjozef.repository.BookRepository;
import com.mediagenix.technicaltestjozef.service.BookService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.sql.Timestamp;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = BookController.class)
@AutoConfigureMockMvc(addFilters = false)
@ExtendWith(MockitoExtension.class)
class BookControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private BookService bookService;

    @MockBean
    private BookRepository bookRepository;

    @Test
    void findAllBooks() throws Exception {
        Book book1 = new Book("a", "a", "a");
        Book book2 = new Book("b", "b", "b");
        List<Book> books = Arrays.asList(book1, book2);

        given(bookService.findAllBooks()).willReturn(books);
        mockMvc.perform(get("/books"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].title", is(books.get(0).getTitle())))
                .andExpect(jsonPath("$[0].isbn", is(books.get(0).getIsbn())))
                .andExpect(jsonPath("$[0].author", is(books.get(0).getAuthor())))
                .andExpect(jsonPath("$[1].title", is(books.get(1).getTitle())))
                .andExpect(jsonPath("$[1].isbn", is(books.get(1).getIsbn())))
                .andExpect(jsonPath("$[1].author", is(books.get(1).getAuthor())))
                .andExpect(jsonPath("$", hasSize(2)));

        verify(bookService, times(1)).findAllBooks();
    }

    @Test
    void findBookById() throws Exception {
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        Book book = new Book(1L, "findBookById-", "isb-findBookById", "author-findBookById", timestamp, timestamp);
        given(bookService.findById(book.getId())).willReturn(Optional.of(book));

        mockMvc.perform(get("/books/1" ))
                .andExpect(status().isOk())
                .andExpect(jsonPath("title", is(book.getTitle())))
                .andExpect(jsonPath("isbn", is(book.getIsbn())))
                .andExpect(jsonPath("author", is(book.getAuthor())));

        verify(bookService, times(1)).findById(book.getId());
    }

    @Test
    void updateBook() throws Exception {
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        Book book = new Book(1L, "title", "isb", "author", timestamp, timestamp);
        Book bookUpdated = new Book(1L, "title-updateBook", "isb-updateBook", "author-updateBook", timestamp, timestamp);

        given(bookService.updateBook(1L, book)).willReturn(book);


        mockMvc.perform(put("/books/1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(book)))
                .andExpect(status().isOk());

        verify(bookService, times(1)).updateBook(1L, book);
    }

    @Test
    void deleteBook() throws Exception {
        doNothing().when(bookService).deleteBook(1L);

        mockMvc.perform(delete("/books/1"))
                .andExpect(status().isOk());

        verify(bookService, times(1)).deleteBook(1L);
    }

    private static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}