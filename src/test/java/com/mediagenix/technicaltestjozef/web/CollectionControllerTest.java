package com.mediagenix.technicaltestjozef.web;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CollectionControllerTest {

    @Test
    void findAllCollections() {
    }

    @Test
    void findCollectionById() {
    }

    @Test
    void findCollectionAndUpdate() {
    }

    @Test
    void saveCollection() {
    }

    @Test
    void deleteCollection() {
    }

    @Test
    void addBookToCollection() {
    }

    @Test
    void removeBookFromCollection() {
    }
}