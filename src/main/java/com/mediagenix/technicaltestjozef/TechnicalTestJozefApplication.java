package com.mediagenix.technicaltestjozef;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TechnicalTestJozefApplication {

	public static void main(String[] args) {
		SpringApplication.run(TechnicalTestJozefApplication.class, args);
	}

}
