package com.mediagenix.technicaltestjozef.model;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "collection")
public class Collection {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "collection_id")
    private Long id;
    @Column(name = "name", unique = true)
    private String name;
    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "book_collection",
            joinColumns = {@JoinColumn(name = "collection_id") },
            inverseJoinColumns = { @JoinColumn(name = "book_id")})
    private List<Book> books;
    @Column(name = "createdAt", updatable = false)
    private Timestamp createdAt;
    @Column(name = "updatedAt")
    private Timestamp updatedAt;

    public Collection() {
        Timestamp now = new Timestamp(System.currentTimeMillis());
        this.books = new ArrayList<>();
        this.createdAt = now;
        this.updatedAt = now;
    }

    public Collection(String name) {
//        this();
        Timestamp now = new Timestamp(System.currentTimeMillis());
        this.books = new ArrayList<>();
        this.createdAt = now;
        this.updatedAt = now;
        this.name = name;
    }

    public Collection(Long id, String name, List<Book> books, Timestamp createdAt, Timestamp updatedAt) {
        this.id = id;
        this.name = name;
        this.books = books;

        if (createdAt == null) {
            Timestamp timestamp = new Timestamp(System.currentTimeMillis());
            this.createdAt = timestamp;
            this.updatedAt = timestamp;
        } else {
            this.createdAt = createdAt;
            this.updatedAt = updatedAt;
        }
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public List<Book> getBooks() {
        return books;
    }

    public Timestamp getCreatedAt() {
        return createdAt;
    }

    public Timestamp getUpdatedAt() {
        return updatedAt;
    }

    public void addBookToCollection(Book b) {
        this.books.add(b);
    }

    public void removeBookFromCollection(Book b) {
        this.books.remove(b);
    }

    public Collection updateCollection(Collection bc) {
        this.name = bc.name;
        this.updatedAt = new Timestamp(System.currentTimeMillis());
        return this;
    }

    @Override
    public String toString() {
        return "Collection{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", books=" + books +
                ", createdAt=" + createdAt +
                ", updatedAt=" + updatedAt +
                '}';
    }
}
