package com.mediagenix.technicaltestjozef.model;

import javax.persistence.*;
import java.sql.Timestamp;
import java.time.ZonedDateTime;

@Entity
@Table(name = "book")
public class Book {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "book_id")
    private Long id;
    @Column(name = "title")
    private String title;
    @Column(name = "isbn", unique = true)
    private String isbn;
    @Column(name = "author")
    private String author;
    @Column(name = "createdAt", updatable = false)
    private Timestamp createdAt;
    @Column(name = "updatedAt")
    private Timestamp updatedAt;

    public Book() {
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        this.createdAt = timestamp;
        this.updatedAt = timestamp;
    }

    public Book(String title, String isbn, String author){
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        this.createdAt = timestamp;
        this.updatedAt = timestamp;
        this.title = title;
        this.isbn = isbn;
        this.author = author;
    }

    public Book(Long id, String title, String isbn, String author, Timestamp createdAt, Timestamp updatedAt) {
        this.id = id;
        this.title = title;
        this.isbn = isbn;
        this.author = author;

        if (createdAt == null) {
            Timestamp timestamp = new Timestamp(System.currentTimeMillis());
            this.createdAt = timestamp;
            this.updatedAt = timestamp;
        } else {
            this.createdAt = createdAt;
            this.updatedAt = updatedAt;
        }
    }

    public Long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getIsbn() {
        return isbn;
    }

    public String getAuthor() {
        return author;
    }

    public Timestamp getCreatedAt() {
        return createdAt;
    }

    public Timestamp getUpdatedAt() {
        return updatedAt;
    }

    public Book updateBook(Book b) {
        this.title = b.title;
        this.author = b.author;
        this.isbn = b.isbn;
        this.updatedAt = new Timestamp(System.currentTimeMillis());
        return this;
    }

    @Override
    public String toString() {
        return "Book{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", isbn='" + isbn + '\'' +
                ", author='" + author + '\'' +
                ", createdAt=" + createdAt +
                ", updatedAt=" + updatedAt +
                '}';
    }
}
