package com.mediagenix.technicaltestjozef.web;

import com.mediagenix.technicaltestjozef.model.Collection;
import com.mediagenix.technicaltestjozef.service.BookService;
import com.mediagenix.technicaltestjozef.service.CollectionService;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/collections")
public class CollectionController {
    private final CollectionService collectionService;

    public CollectionController(CollectionService collectionService, BookService bookService) {
        this.collectionService = collectionService;
    }

    @GetMapping
    public List<Collection> findAllCollections() {
        return collectionService.findAllCollections();
    }

    @GetMapping("/{id}")
    public Optional<Collection> findCollectionById(@PathVariable("id") Long id) {
        return collectionService.findById(id);
    }

    @PutMapping("/{id}")
    public Collection findCollectionAndUpdate(@PathVariable("id") Long id, @RequestBody Collection collection) {
        return collectionService.updateCollection(id, collection);
    }

    @PostMapping()
    public Collection saveCollection(@RequestBody Collection collection) {
        return collectionService.saveCollection(collection);
    }

    @DeleteMapping("/{id}")
    public void deleteCollection(@PathVariable("id") Long id) {
        collectionService.deleteCollection(id);
    }

    @PutMapping("/{collectionId}/addBook/{bookId}")
    public Collection addBookToCollection(@PathVariable Long bookId, @PathVariable Long collectionId) {
        return collectionService.addBookToCollection(bookId, collectionId);
    }

    @PutMapping("/{collectionId}/removeBook/{bookId}")
    public Collection removeBookFromCollection(@PathVariable Long bookId, @PathVariable Long collectionId) {
        return collectionService.removeBookFromCollection(bookId, collectionId);
    }
}