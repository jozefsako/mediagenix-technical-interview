package com.mediagenix.technicaltestjozef.service;

import com.mediagenix.technicaltestjozef.model.Book;
import com.mediagenix.technicaltestjozef.model.Collection;
import com.mediagenix.technicaltestjozef.repository.BookRepository;
import com.mediagenix.technicaltestjozef.repository.CollectionRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CollectionServiceImpl implements CollectionService {

    CollectionRepository collectionRepository;
    BookRepository bookRepository;

    public CollectionServiceImpl(CollectionRepository collectionRepository, BookRepository bookRepository) {
        this.collectionRepository = collectionRepository;
        this.bookRepository = bookRepository;
    }

    @Override
    public List<Collection> findAllCollections() {
        return collectionRepository.findByOrderByName();
    }

    @Override
    public Optional<Collection> findById(Long id) {
        return collectionRepository.findById(id);
    }

    @Override
    public Collection saveCollection(Collection bc) {
        return collectionRepository.save(bc);
    }

    @Override
    public Collection updateCollection(Long id, Collection bc) {
        Collection currentCollection = collectionRepository.findById(id).get();
        Collection updatedCollection = currentCollection.updateCollection(bc);
        return collectionRepository.save(updatedCollection);
    }

    @Override
    public void deleteCollection(Long id) {
        collectionRepository.deleteById(id);
    }

    @Override
    public Collection addBookToCollection(Long bookId, Long collectionId) {
        Collection collection = collectionRepository.findById(collectionId).get();
        Book book = bookRepository.findById(bookId).get();
        collection.addBookToCollection(book);
        return collectionRepository.save(collection);
    }

    @Override
    public Collection removeBookFromCollection(Long bookId, Long collectionId) {
        Collection collection = collectionRepository.findById(collectionId).get();
        Book book = bookRepository.findById(bookId).get();
        collection.removeBookFromCollection(book);
        return collectionRepository.save(collection);
    }
}
