package com.mediagenix.technicaltestjozef.service;

import com.mediagenix.technicaltestjozef.model.Collection;
import java.util.List;
import java.util.Optional;

public interface CollectionService {
    List<Collection> findAllCollections();
    Optional<Collection> findById(Long id);
    Collection saveCollection(Collection bc);
    Collection updateCollection(Long id, Collection bc);
    void deleteCollection(Long id);
    Collection addBookToCollection(Long bookId, Long collectionId);
    Collection removeBookFromCollection(Long bookId, Long collectionId);
}
