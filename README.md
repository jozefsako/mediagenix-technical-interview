# Setup Project


### Setup Postgres Database

In my case I created a local database named: ***mediagenix***

### Replace these values based on your postgres configuraiton
spring.datasource.url=jdbc:postgresql://localhost:5432/mediagenix
spring.datasource.username=postgres
spring.datasource.password=localhost


### API

#### available routes: 

- /collections
  - [GET]     
  - [GET]     /{id}
  - [POST]    
  - [PUT]     /{id}
  - [DELETE]  /{id}
  - [PUT]     /{id}/addBook/{id}
  - [PUT]     /{id}/removeBook/{id}

###
- /books
  - [GET]
  - [GET]     /{id}
  - [POST]
  - [PUT]     /{id}
  - [DELETE]  /{id}
  

### [Git Repository](https://gitlab.com/jozefsako/mediagenix-technical-interview)